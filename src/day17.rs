use std::{
    collections::HashMap,
    io::{BufRead, Write},
    iter,
};

use crate::{
    intcode::{Program, Version},
    point::{Direction, Point},
    util,
};

fn get_move_directions(grid: &HashMap<Point, char>) -> Vec<String> {
    // First get start position
    let (mut pos, mut dir) = grid
        .iter()
        .find_map(|(&p, &c)| match c {
            '>' => Some((p, Direction::Right)),
            '^' => Some((p, Direction::Up)),
            '<' => Some((p, Direction::Left)),
            'v' => Some((p, Direction::Down)),
            _ => None,
        })
        .expect("No start position found");

    let mut run = 0;
    let mut result = vec![];

    loop {
        if grid.get(&pos.move_forward(dir)) == Some(&'#') {
            pos = pos.move_forward(dir);
            run += 1;
        } else {
            if run > 0 {
                result.push(run.to_string());
            }
            run = 0;

            // Look left and right for more scaffolding
            if grid.get(&pos.move_forward(dir.turn_left())) == Some(&'#') {
                dir = dir.turn_left();
                result.push("L".to_owned());
            } else if grid.get(&pos.move_forward(dir.turn_right())) == Some(&'#') {
                dir = dir.turn_right();
                result.push("R".to_owned());
            } else {
                break result;
            }
        }
    }
}

struct DeflateResult {
    a: String,
    b: String,
    c: String,
    stream: String,
}

fn deflate(input: &[String]) -> DeflateResult {
    #[derive(Copy, Clone, Debug)]
    enum Action {
        Apply(usize),
        Add(usize),
    }

    fn str_len(remaining: &[String], size: usize) -> usize {
        remaining[..size].iter().map(|s| s.len()).sum::<usize>() + size - 1
    }

    fn main_stream(trace: &[(Action, &[String])]) -> String {
        trace
            .iter()
            .filter_map(|&(action, _)| match action {
                Action::Apply(0) => Some("A"),
                Action::Apply(1) => Some("B"),
                Action::Apply(2) => Some("C"),
                Action::Apply(_) => unreachable!(),
                Action::Add(_) => None,
            })
            .collect::<Vec<_>>()
            .join(",")
    }

    let mut dict = vec![];
    let mut trace = vec![(Action::Add(1), input)];

    while !trace.is_empty() {
        let &(action, mut remaining) = trace.last().unwrap();

        // Try to actually perform the last action
        let is_applied = match action {
            Action::Apply(idx) => {
                if idx < dict.len() && remaining.starts_with(dict[idx]) {
                    remaining = &remaining[dict[idx].len()..];
                    true
                } else {
                    false
                }
            }
            Action::Add(size) => {
                dict.push(&remaining[..size]);
                //remaining = &remaining[size..];
                true
            }
        };

        if is_applied {
            if remaining.is_empty() && main_stream(&trace).len() <= 20 {
                break;
            }

            // Extend the solution
            trace.push((Action::Apply(0), remaining));
        } else {
            // Try next action or remove it all together
            'backtrack: while let Some((action, remaining)) = trace.pop() {
                match action {
                    Action::Apply(idx) => {
                        if idx + 1 < 3 {
                            trace.push((Action::Apply(idx + 1), remaining));
                            break 'backtrack;
                        } else if dict.len() < 3 {
                            trace.push((Action::Add(1), remaining));
                            break 'backtrack;
                        }
                    }
                    Action::Add(size) => {
                        dict.pop();
                        if size < remaining.len() && str_len(remaining, size + 1) <= 20 {
                            trace.push((Action::Add(size + 1), remaining));
                            break 'backtrack;
                        }
                    }
                }
            }
        }
    }

    assert!(!trace.is_empty()); // Would happen if no solution found

    DeflateResult {
        a: dict.get(0).copied().unwrap_or(&[]).join(","),
        b: dict.get(1).copied().unwrap_or(&[]).join(","),
        c: dict.get(2).copied().unwrap_or(&[]).join(","),
        stream: main_stream(&trace),
    }
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut prog = Program::parse(&util::read_line(&mut input), Version::Day09);
    let mut prog2 = prog.clone();
    let prog_output = prog.execute_with_input(0, iter::empty()).unwrap();

    //print!("{}", &prog_output.iter().map(|&x| x as u8 as char).collect::<String>());

    let grid: HashMap<_, _> = prog_output
        .iter()
        .copied()
        .scan(Point { x: 0, y: 0 }, |state, c| {
            let c = c as u8 as char;
            let ret = (*state, c);
            if c == '\n' {
                state.x = 0;
                state.y -= 1;
            } else {
                state.x += 1;
            }
            Some(ret)
        })
        .filter(|&(_, c)| c != '\n')
        .collect();

    match part {
        1 => {
            let mut alignment = 0;
            for point in grid
                .iter()
                .filter_map(|(&point, &c)| if c == '#' { Some(point) } else { None })
            {
                if point
                    .neighbours()
                    .all(|neighbour| grid.get(&neighbour) == Some(&'#'))
                {
                    alignment += point.x * -point.y;
                }
            }

            writeln!(output, "{}", alignment).unwrap();
        }
        2 => {
            let move_directions = get_move_directions(&grid);
            let deflated = deflate(&move_directions);
            let prog_input = format!(
                "{}\n{}\n{}\n{}\nn\n",
                deflated.stream, deflated.a, deflated.b, deflated.c
            );

            prog2.write_cell(0, 2).unwrap();
            let prog_output = prog2
                .execute_with_input(0, prog_input.chars().map(|c| c as i64))
                .unwrap();

            //print!("{}", &prog_output.iter().map(|&x| x as u8 as char).collect::<String>());
            writeln!(output, "{}", prog_output.last().unwrap()).unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day17.txt").unwrap(), "4864");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day17.txt").unwrap(), "840248");
    }
}
