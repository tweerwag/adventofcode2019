use std::collections::HashMap;
use std::io::{BufRead, Write};
use std::iter;

use crate::intcode::{ExecutionResult, Program, Version};
use crate::util;

// fn display_board(board: &HashMap<(i64, i64), i64>) {
//     let &min_x = board.keys().map(|(x, _)| x).min().unwrap();
//     let &max_x = board.keys().map(|(x, _)| x).max().unwrap();
//     let &min_y = board.keys().map(|(_, y)| y).min().unwrap();
//     let &max_y = board.keys().map(|(_, y)| y).max().unwrap();
//     for y in min_y..=max_y {
//         for x in min_x..=max_x {
//             let c = match board.get(&(x, y)).copied().unwrap_or(0) {
//                 0 => ' ',
//                 1 => '%',
//                 2 => '#',
//                 3 => '-',
//                 4 => 'O',
//                 _ => '?',
//             };
//             print!("{}", c);
//         }
//         println!();
//     }
//     println!();
// }

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut prog = Program::parse(&util::read_line(&mut input), Version::Day09);

    if part == 2 {
        prog.write_cell(0, 2).unwrap();
    }

    let mut executor = prog.executor(0);
    let mut board = HashMap::new();
    let mut score = 0;
    let mut tilt = 0;

    loop {
        let x = match executor.run(&mut iter::once(tilt)).unwrap() {
            ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
            ExecutionResult::OutputProduced(x) => x,
            ExecutionResult::Halted => break,
        };
        let y = match executor.run(&mut iter::empty()).unwrap() {
            ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
            ExecutionResult::OutputProduced(y) => y,
            ExecutionResult::Halted => panic!("Unexpected halt"),
        };
        let tile_id = match executor.run(&mut iter::empty()).unwrap() {
            ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
            ExecutionResult::OutputProduced(tile_id) => tile_id,
            ExecutionResult::Halted => panic!("Unexpected halt"),
        };

        // We need to put our joystick in neutral each time, since the game
        // toggles on edges.
        tilt = 0;

        if x == -1 && y == 0 {
            score = tile_id;
        } else {
            board.insert((x, y), tile_id);

            // Only create input if the ball was drawn. Somehow, this way we're
            // synchronized.
            if tile_id == 4 {
                let ball_x = board
                    .iter()
                    .find_map(|((x, _), &tile_id)| if tile_id == 4 { Some(x) } else { None });
                let paddle_x = board
                    .iter()
                    .find_map(|((x, _), &tile_id)| if tile_id == 3 { Some(x) } else { None });
                if let Some(ball_x) = ball_x {
                    if let Some(paddle_x) = paddle_x {
                        tilt = ((ball_x - paddle_x) as i64).signum();
                    }
                }
            }
        }
    }

    match part {
        1 => {
            let num_blocks = board.values().filter(|&&tile_id| tile_id == 2).count();
            writeln!(output, "{}", num_blocks).unwrap();
        }
        2 => writeln!(output, "{}", score).unwrap(),
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day13.txt").unwrap(), "304");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day13.txt").unwrap(), "14747");
    }
}
