use std::collections::HashSet;
use std::hash::Hash;
use std::io;

pub fn read_line<T: io::BufRead>(reader: &mut T) -> String {
    let mut line = String::new();
    reader
        .read_line(&mut line)
        .expect("Could not read line from stdin");
    String::from(line.trim())
}

pub struct Digits(i64);

impl Iterator for Digits {
    type Item = i64;

    fn next(&mut self) -> Option<i64> {
        if self.0 > 0 {
            let digit = self.0 % 10;
            self.0 /= 10;
            Some(digit)
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(10))
    }
}

pub trait I64Extras {
    fn digits(self) -> Digits;
}

impl I64Extras for i64 {
    fn digits(self) -> Digits {
        Digits(self)
    }
}

pub struct RunLengthEncoder<'a, T, I> {
    iter: &'a mut I,
    cur_item: Option<T>,
}

impl<'a, T: Copy + PartialEq, I: Iterator<Item = T>> Iterator for RunLengthEncoder<'a, T, I> {
    type Item = (T, usize);

    fn next(&mut self) -> Option<(T, usize)> {
        if let Some(cur_item) = self.cur_item {
            let mut run_length = 1;
            loop {
                let next_item = self.iter.next();
                match next_item {
                    Some(next_item) if next_item == cur_item => {
                        run_length += 1;
                    }
                    other => {
                        self.cur_item = other;
                        break;
                    }
                }
            }
            Some((cur_item, run_length))
        } else {
            None
        }
    }
}

pub trait IteratorRunLengthEncoding: Iterator + Sized {
    fn run_length_encoding(&mut self) -> RunLengthEncoder<Self::Item, Self>;
}

impl<T: Copy + PartialEq, I: Iterator<Item = T>> IteratorRunLengthEncoding for I {
    fn run_length_encoding(&mut self) -> RunLengthEncoder<T, I> {
        let cur_item = self.next();
        RunLengthEncoder {
            iter: self,
            cur_item,
        }
    }
}

pub fn find_first_common<'a, T: Eq + Hash>(slice1: &'a [T], slice2: &'a [T]) -> Option<&'a T> {
    let commons = &slice1.iter().collect::<HashSet<_>>() & &slice2.iter().collect::<HashSet<_>>();
    slice1.iter().find(|x| commons.contains(x))
}

pub struct Permutator {
    started: bool,
    cur: Vec<usize>,
}

impl Permutator {
    pub fn new<I: IntoIterator<Item = usize>>(into_iter: I) -> Permutator {
        let mut v = into_iter.into_iter().collect::<Vec<_>>();
        v.sort();
        Permutator {
            started: false,
            cur: v,
        }
    }
}

impl Iterator for Permutator {
    // Meh, unfortunately we cannot return a slice-ref into our own vec. See "streaming iterator".
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Vec<usize>> {
        if !self.started {
            self.started = true;
            return Some(self.cur.clone());
        }

        let (pivot_index, pivot, _) = self
            .cur
            .iter()
            .enumerate()
            .rev()
            .scan(None, |state, (idx, &cur)| {
                let is_smaller;
                if let Some(prev) = *state {
                    is_smaller = cur < prev;
                } else {
                    is_smaller = false;
                }
                *state = Some(cur);
                Some((idx, cur, is_smaller))
            })
            .find(|&(_, _, c)| c)?;

        let (swap_index, _) = self
            .cur
            .iter()
            .enumerate()
            .skip(pivot_index + 1)
            .filter(|(_, &x)| x > pivot)
            .min_by(|(_, x), (_, y)| x.cmp(y))
            .unwrap();
        self.cur.swap(pivot_index, swap_index);

        self.cur[(pivot_index + 1)..].reverse();

        Some(self.cur.clone())
    }
}

pub fn gcd(mut x: usize, mut y: usize) -> usize {
    while y != 0 {
        let t = y;
        y = x % y;
        x = t;
    }
    x
}

pub fn lcm(x: usize, y: usize) -> usize {
    (x / gcd(x, y)) * y
}

pub mod prelude {
    pub use super::{I64Extras, IteratorRunLengthEncoding};
}

#[macro_export]
macro_rules! assert_io_eq {
    ($part:expr, $input:expr, $output:expr) => {
        let mut inp_buf = ::std::io::Cursor::new(String::from($input));
        let mut out_buf = ::std::io::Cursor::new(vec![]);
        super::execute($part, &mut inp_buf, &mut out_buf);
        assert_eq!(
            String::from_utf8(out_buf.into_inner()).unwrap().trim(),
            $output
        );
    };
}
