use std::io::{BufRead, Write};
use std::iter;

use crate::intcode::{ExecutionResult, Program, Version};
use crate::util;

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let prog = Program::parse(&util::read_line(&mut input), Version::Day05);

    let max_thruster = match part {
        1 => util::Permutator::new(0..5)
            .map(|perm| {
                perm.iter().fold(0, |input, &phase| {
                    let mut prog = prog.clone();
                    let output = prog
                        .execute_with_input(0, iter::once(phase as i64).chain(iter::once(input)));
                    output.unwrap()[0]
                })
            })
            .max()
            .unwrap(),
        2 => util::Permutator::new(5..10)
            .map(|perm| {
                let mut progs = perm.iter().map(|_| prog.clone()).collect::<Vec<_>>();
                let mut execs = progs.iter_mut().map(|p| p.executor(0)).collect::<Vec<_>>();

                for (exec, &phase) in execs.iter_mut().zip(perm.iter()) {
                    let mut input_iter = iter::once(phase as i64);
                    assert_eq!(
                        exec.run(&mut input_iter).unwrap(),
                        ExecutionResult::WaitingForInput
                    );
                }

                let mut signal = 0;
                let mut idx = 0;
                loop {
                    let mut input_iter = iter::once(signal);
                    let res = execs[idx].run(&mut input_iter).unwrap();

                    match res {
                        ExecutionResult::WaitingForInput => panic!("Unexpected waiting for input"),
                        ExecutionResult::OutputProduced(new_signal) => signal = new_signal,
                        ExecutionResult::Halted => break signal,
                    }

                    idx += 1;
                    if idx == execs.len() {
                        idx = 0;
                    }
                }
            })
            .max()
            .unwrap(),
        _ => unreachable!(),
    };
    writeln!(output, "{}", max_thruster).unwrap();
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn examples_part1() {
        assert_io_eq!(1, "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", "43210");
        assert_io_eq!(
            1,
            "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",
            "54321"
        );
        assert_io_eq!(1, "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", "65210");
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day07.txt").unwrap(), "67023");
    }

    #[test]
    fn examples_part2() {
        assert_io_eq!(
            2,
            "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5",
            "139629729"
        );
        assert_io_eq!(
            2,
            "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10",
            "18216"
        );
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day07.txt").unwrap(), "7818398");
    }
}
