use std::collections::HashMap;
use std::io::{BufRead, Write};
use std::iter;

use crate::intcode::{ExecutionResult, Program, Version};
use crate::point::{Direction, Point};
use crate::util;

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut prog = Program::parse(&util::read_line(&mut input), Version::Day09);
    let mut executor = prog.executor(0);
    let mut hull = HashMap::new();
    let mut current_position = Point { x: 0, y: 0 };
    let mut current_direction = Direction::Up;

    if part == 2 {
        hull.insert(current_position, 1);
    }

    loop {
        let current_colour = hull.get(&current_position).copied().unwrap_or(0);
        match executor.run(&mut iter::once(current_colour)).unwrap() {
            ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
            ExecutionResult::OutputProduced(colour) => {
                hull.insert(current_position, colour);
            }
            ExecutionResult::Halted => break,
        }
        match executor.run(&mut iter::empty()).unwrap() {
            ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
            ExecutionResult::OutputProduced(turn) => match turn {
                0 => current_direction = current_direction.turn_left(),
                1 => current_direction = current_direction.turn_right(),
                _ => panic!("Unknown direction"),
            },
            ExecutionResult::Halted => panic!("Unexpected halt"),
        }
        current_position = current_position.move_forward(current_direction);
    }

    match part {
        1 => writeln!(output, "{}", hull.len()).unwrap(),
        2 => {
            let &min_x = hull.keys().map(|Point { x, .. }| x).min().unwrap();
            let &max_x = hull.keys().map(|Point { x, .. }| x).max().unwrap();
            let &min_y = hull.keys().map(|Point { y, .. }| y).min().unwrap();
            let &max_y = hull.keys().map(|Point { y, .. }| y).max().unwrap();
            for y in (min_y..=max_y).rev() {
                for x in min_x..=max_x {
                    let c = match hull.get(&Point { x, y }).copied().unwrap_or(0) {
                        0 => '.',
                        1 => '#',
                        _ => '?',
                    };
                    write!(output, "{}", c).unwrap();
                }
                writeln!(output).unwrap();
            }
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day11.txt").unwrap(), "2211");
    }
}
