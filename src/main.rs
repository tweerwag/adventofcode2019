mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day20;

mod graph;
mod intcode;
//mod matrix;
mod point;
mod point3;
mod rational;
#[macro_use]
mod util;

fn main() {
    let matches = clap::App::new("Advent of Code 2019")
        .author("Timmy Weerwag")
        .arg(
            clap::Arg::with_name("DAY")
                .required(true)
                .help("Specifies the problem number"),
        )
        .arg(
            clap::Arg::with_name("PART")
                .required(true)
                .help("Specifies the part of the problem"),
        )
        .get_matches();

    let part = match matches.value_of("PART").unwrap().parse::<i32>().ok() {
        Some(part @ 1..=2) => part,
        _ => panic!(),
    };
    match matches.value_of("DAY").unwrap().parse::<i32>().ok() {
        Some(1) => day01::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(2) => day02::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(3) => day03::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(4) => day04::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(5) => day05::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(6) => day06::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(7) => day07::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(8) => day08::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(9) => day09::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(10) => day10::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(11) => day11::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(12) => day12::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(13) => day13::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(14) => day14::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(15) => day15::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(16) => day16::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(17) => day17::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(18) => day18::execute(part, std::io::stdin().lock(), std::io::stdout()),
        Some(20) => day20::execute(part, std::io::stdin().lock(), std::io::stdout()),
        _ => panic!("DAY must be a valid problem number"),
    }
}
