use std::cmp::Ordering;
use std::collections::HashMap;
use std::io::{BufRead, Write};

use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, digit1, newline},
    combinator::{map, map_res},
    multi::{fold_many0, separated_list},
    sequence::{separated_pair, terminated},
    IResult,
};

type Inventory = HashMap<String, i64>;

type ReactionSet = HashMap<String, (i64, Inventory)>;

fn parse_num(input: &str) -> IResult<&str, i64> {
    map_res(digit1, |input: &str| input.parse::<i64>())(input)
}

fn parse_name(input: &str) -> IResult<&str, String> {
    map(alpha1, |s: &str| s.to_owned())(input)
}

fn parse_product(input: &str) -> IResult<&str, (i64, String)> {
    separated_pair(parse_num, tag(" "), parse_name)(input)
}

fn parse_inventory(input: &str) -> IResult<&str, Inventory> {
    map(
        separated_list(tag(", "), parse_product),
        |prods: Vec<(i64, String)>| {
            prods
                .into_iter()
                .map(|(num, s)| (s, num))
                .collect::<Inventory>()
        },
    )(input)
}

fn parse_reaction(input: &str) -> IResult<&str, (Inventory, (i64, String))> {
    separated_pair(parse_inventory, tag(" => "), parse_product)(input)
}

fn parse_reaction_set(input: &str) -> IResult<&str, ReactionSet> {
    fold_many0(
        terminated(parse_reaction, newline),
        ReactionSet::new(),
        |mut set, (inv, (num, output))| {
            set.insert(output, (num, inv));
            set
        },
    )(input)
}

fn find_needed_product(inventory: &Inventory) -> Option<(i64, String)> {
    inventory
        .iter()
        .filter(|(s, _)| s != &"ORE")
        .find(|(_, &num)| num < 0)
        .map(|(s, &num)| (num, s.clone()))
}

fn ore_for_fuel(reaction_set: &ReactionSet, fuel: i64) -> i64 {
    let mut inventory = Inventory::new();
    inventory.insert("FUEL".to_owned(), -fuel);

    while let Some((num_needed, prod_needed)) = find_needed_product(&inventory) {
        let (num_produced, ref reagents) = reaction_set[&prod_needed];
        let num_reactions = (-num_needed + (num_produced - 1)) / num_produced;

        // println!("Looking for {}", &prod_needed);
        // println!("--> Found reaction");
        // println!("--> Produces {}", num_produced);
        // println!("--> Needs {:?}", reagents);
        // println!("--> Reacting {} times", num_reactions);

        reagents
            .iter()
            .map(|(s, &num)| (num, &s as &str))
            .for_each(|(num, s)| {
                let entry = inventory.entry(s.to_owned()).or_insert(0);
                *entry -= num * num_reactions;
            });
        *inventory.get_mut(&prod_needed).unwrap() += num_produced * num_reactions;

        // println!("--> Inventory after: {:?}", &inventory);
    }

    -inventory["ORE"]
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let reaction_set = {
        let mut input_str = String::new();
        input.read_to_string(&mut input_str).unwrap();
        let (leftover_input, reaction_set) = parse_reaction_set(&input_str).unwrap();
        assert!(
            leftover_input.is_empty(),
            "Found garbage at end of input: {:?}",
            &leftover_input
        );
        reaction_set
    };

    match part {
        1 => {
            writeln!(output, "{}", ore_for_fuel(&reaction_set, 1)).unwrap();
        }
        2 => {
            let ore_available = 1_000_000_000_000;

            // Find a lower bound
            let mut lower = ore_available / ore_for_fuel(&reaction_set, 1);

            // Find an upper bound
            let mut upper = lower;
            while ore_for_fuel(&reaction_set, upper) < ore_available {
                upper *= 2;
            }

            // Do the binary search
            while lower + 1 < upper {
                let guess = (upper + lower) / 2;
                let ore = ore_for_fuel(&reaction_set, guess);

                match ore.cmp(&ore_available) {
                    Ordering::Less => lower = guess,
                    Ordering::Greater => upper = guess,
                    Ordering::Equal => {
                        lower = guess;
                        break;
                    }
                }
            }

            writeln!(output, "{}", lower).unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn examples_part1() {
        assert_io_eq!(
            1,
            "10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL
",
            "31"
        );
        assert_io_eq!(
            1,
            "9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL
",
            "165"
        );
        assert_io_eq!(
            1,
            "157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT
",
            "13312"
        );
        assert_io_eq!(
            1,
            "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF
",
            "180697"
        );
        assert_io_eq!(
            1,
            "171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX
",
            "2210736"
        );
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day14.txt").unwrap(), "248794");
    }

    #[test]
    fn examples_part2() {
        assert_io_eq!(
            2,
            "157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT
",
            "82892753"
        );
        assert_io_eq!(
            2,
            "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF
",
            "5586022"
        );
        assert_io_eq!(
            2,
            "171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX
",
            "460664"
        );
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day14.txt").unwrap(), "4906796");
    }
}
