use std::ops;

#[derive(Hash, Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Point3<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

macro_rules! point3_impl {
    ($($t:ty)*) => {$(
        impl Point3<$t> {
            pub fn manhattan_norm(self) -> $t {
                self.x.abs() + self.y.abs() + self.z.abs()
            }
        }

        impl Default for Point3<$t> {
            fn default() -> Point3<$t> {
                Point3 {
                    x: 0,
                    y: 0,
                    z: 0,
                }
            }
        }

        impl ops::Add for Point3<$t> {
            type Output = Point3<$t>;

            fn add(self, rhs: Point3<$t>) -> Point3<$t> {
                Point3 {
                    x: self.x + rhs.x,
                    y: self.y + rhs.y,
                    z: self.z + rhs.z,
                }
            }
        }

        impl ops::AddAssign for Point3<$t> {
            fn add_assign(&mut self, rhs: Point3<$t>) {
                self.x += rhs.x;
                self.y += rhs.y;
                self.z += rhs.z;
            }
        }

        impl ops::Sub for Point3<$t> {
            type Output = Point3<$t>;

            fn sub(self, rhs: Point3<$t>) -> Point3<$t> {
                Point3 {
                    x: self.x - rhs.x,
                    y: self.y - rhs.y,
                    z: self.z - rhs.z,
                }
            }
        }

        impl ops::SubAssign for Point3<$t> {
            fn sub_assign(&mut self, rhs: Point3<$t>) {
                self.x -= rhs.x;
                self.y -= rhs.y;
                self.z -= rhs.z;
            }
        }

        impl ops::Neg for Point3<$t> {
            type Output = Point3<$t>;

            fn neg(self) -> Point3<$t> {
                Point3 {
                    x: -self.x,
                    y: -self.y,
                    z: -self.z,
                }
            }
        }
    )*}
}

point3_impl! { i64 }
