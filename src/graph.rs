use std::{
    cmp,
    collections::{HashMap, HashSet},
    hash::Hash,
};

use priority_queue::PriorityQueue;

pub struct Graph<N> {
    adjacency_list: HashMap<N, Vec<N>>,
}

impl<N: Eq + Hash> Graph<N> {
    pub fn from_edges<I>(into_iter: I) -> Graph<N>
    where
        I: IntoIterator<Item = (N, N)>,
    {
        let mut graph = Graph {
            adjacency_list: HashMap::new(),
        };
        for (left, right) in into_iter.into_iter() {
            graph.adjacency_list.entry(left).or_default().push(right);
        }
        graph
    }
}

pub fn dijkstra<N>(graph: &Graph<N>, start: N, goals: &[N]) -> Option<(N, usize)>
where
    N: Copy + Eq + Hash,
{
    dijkstra_by(
        start,
        |n| goals.contains(&n),
        |n| {
            graph
                .adjacency_list
                .get(&n)
                .map(|x| x as &[N])
                .unwrap_or(&[])
                .iter()
                .map(|&n| (n, 1))
        },
    )
}

pub fn dijkstra_by<N, FG, I, FA>(start: N, is_goal: FG, adjacent_func: FA) -> Option<(N, usize)>
where
    N: Copy + Eq + Hash,
    FG: Fn(N) -> bool,
    I: IntoIterator<Item = (N, usize)>,
    FA: Fn(N) -> I,
{
    let mut visited = HashSet::new();
    let mut unvisited = PriorityQueue::new();
    unvisited.push(start, cmp::Reverse(0));

    while let Some((node, cmp::Reverse(cost))) = unvisited.pop() {
        if is_goal(node) {
            return Some((node, cost));
        }

        for (neighbour, weight) in adjacent_func(node) {
            if !visited.contains(&neighbour) {
                let new_cost = unvisited
                    .get_priority(&neighbour)
                    .map(|&cmp::Reverse(other_cost)| usize::min(other_cost, cost + weight))
                    .unwrap_or(cost + weight);
                unvisited.push(neighbour, cmp::Reverse(new_cost));
            }
        }

        visited.insert(node);
    }

    None
}

const INFINITY: usize = std::usize::MAX;
pub fn dijkstra_old<N>(graph: &Graph<N>, start: N, goals: &[N]) -> Option<(N, usize)>
where
    N: Copy + Eq + Hash,
{
    let mut last_cost = INFINITY;
    let mut unvisited: PriorityQueue<_, _> = graph
        .adjacency_list
        .keys()
        .map(|&node| (node, cmp::Reverse(INFINITY)))
        .collect();
    unvisited.change_priority(&start, cmp::Reverse(0));

    while let Some((node, cmp::Reverse(cost))) = unvisited.pop() {
        if goals.contains(&node) {
            return Some((node, cost));
        }
        if cost == INFINITY {
            return None;
        }

        for &neighbour in &graph.adjacency_list[&node] {
            if unvisited.get(&neighbour).is_some() {
                unvisited.change_priority_by(&neighbour, |cmp::Reverse(other_cost)| {
                    cmp::Reverse(usize::min(other_cost, cost + 1))
                });
            }
        }

        last_cost = cost;
    }

    if goals.is_empty() {
        Some((start, last_cost))
    } else {
        None
    }
}
