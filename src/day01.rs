use std::io::{BufRead, Write};

fn compute_single_fuel(mass: i32) -> i32 {
    mass / 3 - 2
}

fn compute_recursive_fuel(mass: i32) -> i32 {
    let mut fuel = compute_single_fuel(mass);
    let mut total_fuel = 0;
    while fuel > 0 {
        total_fuel += fuel;
        fuel = compute_single_fuel(fuel);
    }
    total_fuel
}

pub fn execute(part: i32, input: impl BufRead, mut output: impl Write) {
    let compute_fuel: &dyn Fn(i32) -> i32 = match part {
        1 => &compute_single_fuel,
        2 => &compute_recursive_fuel,
        _ => panic!("invalid part"),
    };
    let required_fuel: i32 = input
        .lines()
        .map(|x| x.unwrap())
        .filter_map(|x| x.parse::<i32>().ok())
        .map(compute_fuel)
        .sum();
    writeln!(output, "{}", required_fuel).unwrap();
}

#[cfg(test)]
mod tests {
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn examples() {
        assert_io_eq!(1, "12", "2");
        assert_io_eq!(1, "14", "2");
        assert_io_eq!(1, "1969", "654");
        assert_io_eq!(1, "100756", "33583");
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day01.txt").unwrap(), "3442987");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day01.txt").unwrap(), "5161601");
    }
}
