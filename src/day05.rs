use std::io::{BufRead, Write};

use crate::intcode::{Program, Version};
use crate::util;

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut program = Program::parse(&util::read_line(&mut input), Version::Day05);
    let prog_input = match part {
        1 => vec![1],
        2 => vec![5],
        _ => unreachable!(),
    };
    let prog_output = program.execute_with_input(0, prog_input).unwrap();
    writeln!(output, "{}", prog_output.last().unwrap()).unwrap();
}

#[cfg(test)]
mod tests {
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day05.txt").unwrap(), "13294380");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day05.txt").unwrap(), "11460760");
    }
}
