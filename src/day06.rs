use std::collections::HashSet;
use std::io::{BufRead, Write};

use crate::util;

#[derive(Debug)]
struct Map {
    edges: Vec<(String, String)>,
}

impl Map {
    fn parse(input: impl BufRead) -> Map {
        Map {
            edges: input
                .lines()
                .map(Result::unwrap)
                .map(|line| {
                    let mut iter = line.split(')').map(String::from);
                    let left = iter.next().unwrap();
                    let right = iter.next().unwrap();
                    (left, right)
                })
                .collect(),
        }
    }

    fn children<'a>(&'a self, node: &'a str) -> impl Iterator<Item = &'a str> {
        let node = String::from(node);
        self.edges
            .iter()
            .filter(move |(s, _)| s == node.as_str())
            .map(|(_, s)| s.as_str())
    }

    fn parent<'a>(&'a self, node: &'a str) -> Option<&'a str> {
        let node = String::from(node);
        self.edges
            .iter()
            .filter(move |(_, s)| s == node.as_str())
            .map(|(s, _)| s.as_str())
            .next()
    }

    fn find_roots(&self) -> Vec<&str> {
        let keys = self
            .edges
            .iter()
            .map(|(s, _)| s.as_str())
            .collect::<HashSet<_>>();
        let values = self
            .edges
            .iter()
            .map(|(_, s)| s.as_str())
            .collect::<HashSet<_>>();
        keys.difference(&values).copied().collect()
    }

    fn get_node_depths(&self) -> Vec<(&str, usize)> {
        fn aux<'a>(
            map: &'a Map,
            node_depths: &mut Vec<(&'a str, usize)>,
            cur_node: &'a str,
            cur_level: usize,
        ) {
            node_depths.push((cur_node, cur_level));
            for node in map.children(cur_node) {
                aux(map, node_depths, node, cur_level + 1);
            }
        }

        let mut node_depths = vec![];
        for root in self.find_roots() {
            aux(self, &mut node_depths, root, 0);
        }
        node_depths
    }

    fn get_path_to_root<'a>(&'a self, mut node: &'a str) -> Vec<&'a str> {
        let mut path = vec![];
        while let Some(parent) = self.parent(node) {
            path.push(parent);
            node = parent;
        }
        path
    }
}

pub fn execute(part: i32, input: impl BufRead, mut output: impl Write) {
    let map = Map::parse(input);
    match part {
        1 => {
            let node_depths = map.get_node_depths();
            writeln!(
                output,
                "{}",
                node_depths.into_iter().map(|(_, d)| d).sum::<usize>()
            )
            .unwrap();
        }
        2 => {
            let path1 = map.get_path_to_root("YOU");
            let path2 = map.get_path_to_root("SAN");
            let first_common =
                util::find_first_common(&path1, &path2).expect("Universe is misformed");
            let transfers_needed = path1.iter().take_while(|&x| x != first_common).count()
                + path2.iter().take_while(|&x| x != first_common).count();
            writeln!(output, "{}", transfers_needed).unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn example_part1() {
        assert_io_eq!(
            1,
            "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L",
            "42"
        );
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day06.txt").unwrap(), "278744");
    }

    #[test]
    fn example_part2() {
        assert_io_eq!(
            2,
            "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN",
            "4"
        );
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day06.txt").unwrap(), "475");
    }
}
