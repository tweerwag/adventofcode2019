use std::iter;

use crate::util::prelude::*;

mod error;
use error::{Error, ErrorKind, Result};

const INSTRUCTION_MAX_ARGUMENTS: usize = 3;

#[derive(Clone, Copy, PartialEq)]
pub enum Version {
    Day02,
    Day05,
    Day09,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Op {
    Add,
    Mul,
    Halt,
    Input,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equals,
    RelativeBaseOffset,
    Invalid,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum AddressingMode {
    Position,
    Immediate,
    Relative,
    Invalid,
}

#[derive(Clone, Debug)]
pub struct OpCode {
    op: Op,
    addressing_modes: [AddressingMode; INSTRUCTION_MAX_ARGUMENTS],
}

impl OpCode {
    fn is_valid_for_version(&self, version: Version) -> bool {
        let num = match version {
            Version::Day02 => 2,
            Version::Day05 => 5,
            Version::Day09 => 9,
        };

        // First check for actual invalid
        if self.op == Op::Invalid || self.addressing_modes.contains(&AddressingMode::Invalid) {
            return false;
        }

        // Check day05 features
        if num < 5 {
            if self
                .addressing_modes
                .iter()
                .any(|&mode| mode == AddressingMode::Immediate)
            {
                return false;
            }

            if self.op == Op::Input
                || self.op == Op::Output
                || self.op == Op::JumpIfTrue
                || self.op == Op::JumpIfFalse
                || self.op == Op::LessThan
                || self.op == Op::Equals
            {
                return false;
            }
        }

        // Check day09 features
        if num < 9 {
            if self
                .addressing_modes
                .iter()
                .any(|&mode| mode == AddressingMode::Relative)
            {
                return false;
            }

            if self.op == Op::RelativeBaseOffset {
                return false;
            }
        }

        true
    }

    fn instruction_length(&self) -> i64 {
        match self.op {
            Op::Add | Op::Mul => 4,
            Op::Halt => 1,
            Op::Input | Op::Output => 2,
            Op::JumpIfTrue | Op::JumpIfFalse => 3,
            Op::LessThan | Op::Equals => 4,
            Op::RelativeBaseOffset => 2,
            Op::Invalid => panic!("Invalid op"),
        }
    }
}

impl From<i64> for OpCode {
    fn from(code: i64) -> OpCode {
        let op_value = code % 100;
        let modes_values = code / 100;

        let op = match op_value {
            1 => Op::Add,
            2 => Op::Mul,
            3 => Op::Input,
            4 => Op::Output,
            5 => Op::JumpIfTrue,
            6 => Op::JumpIfFalse,
            7 => Op::LessThan,
            8 => Op::Equals,
            9 => Op::RelativeBaseOffset,
            99 => Op::Halt,
            _ => Op::Invalid,
        };

        let mut modes_digits = modes_values.digits();
        let mut parse_mode = || {
            modes_digits
                .next()
                .map(|x| match x {
                    0 => AddressingMode::Position,
                    1 => AddressingMode::Immediate,
                    2 => AddressingMode::Relative,
                    _ => AddressingMode::Invalid,
                })
                .unwrap_or(AddressingMode::Position)
        };
        let mode_arg1 = parse_mode();
        let mode_arg2 = parse_mode();
        let mode_arg3 = parse_mode();

        OpCode {
            op,
            addressing_modes: [mode_arg1, mode_arg2, mode_arg3],
        }
    }
}

#[derive(Clone)]
pub struct Program {
    memory: Vec<i64>,
    version: Version,
}

impl Program {
    pub fn parse(s: &str, version: Version) -> Program {
        Program {
            memory: s.split(',').filter_map(|x| x.parse::<i64>().ok()).collect(),
            version,
        }
    }

    #[cfg(test)]
    pub fn get_memory(&self) -> &[i64] {
        &self.memory
    }

    pub fn read_cell(&mut self, index: i64) -> Result<i64> {
        if index as usize + 1 > self.memory.len() {
            self.memory.resize(index as usize + 1, 0);
        }
        self.memory
            .get(index as usize)
            .copied()
            .ok_or(Error::Operation(ErrorKind::MemoryAccessOutOfBounds, None))
    }

    pub fn write_cell(&mut self, index: i64, value: i64) -> Result<()> {
        if index as usize + 1 > self.memory.len() {
            self.memory.resize(index as usize + 1, 0);
        }
        let cell = self
            .memory
            .get_mut(index as usize)
            .ok_or(Error::Operation(ErrorKind::MemoryAccessOutOfBounds, None))?;
        *cell = value;
        Ok(())
    }

    pub fn size(&self) -> usize {
        self.memory.len()
    }

    pub fn execute(&mut self, start_address: i64) -> Result<()> {
        self.execute_with_input(start_address, iter::empty())
            .map(|_| ())
    }

    pub fn execute_with_input<I>(&mut self, start_address: i64, input: I) -> Result<Vec<i64>>
    where
        I: IntoIterator<Item = i64>,
    {
        let mut executor = self.executor(start_address);
        let mut input_iter = input.into_iter();
        let mut output = vec![];

        while let ExecutionResult::OutputProduced(val) = executor.run(&mut input_iter)? {
            output.push(val);
        }
        Ok(output)
    }

    pub fn executor(&mut self, start_address: i64) -> Executor {
        Executor {
            prog: self,
            counter: start_address,
            relative_base: 0,
        }
    }
}

pub struct Executor<'a> {
    prog: &'a mut Program,
    counter: i64,
    relative_base: i64,
}

#[derive(PartialEq, Eq, Debug)]
pub enum ExecutionResult {
    WaitingForInput,
    OutputProduced(i64),
    Halted,
}

impl<'a> Executor<'a> {
    fn read_argument(&mut self, opcode: &OpCode, number: usize) -> Result<i64> {
        let address = self.counter + number as i64;
        let mode = opcode.addressing_modes[number - 1];
        match mode {
            AddressingMode::Position => {
                let address = self.prog.read_cell(address)?;
                self.prog.read_cell(address)
            }
            AddressingMode::Immediate => self.prog.read_cell(address),
            AddressingMode::Relative => {
                let address = self.relative_base + self.prog.read_cell(address)?;
                self.prog.read_cell(address)
            }
            AddressingMode::Invalid => Err(Error::Operation(
                ErrorKind::InvalidAddressingMode,
                Some(opcode.clone()),
            )),
        }
    }

    fn write_argument(&mut self, opcode: &OpCode, number: usize, value: i64) -> Result<()> {
        let address = self.counter + number as i64;
        let mode = opcode.addressing_modes[number - 1];
        match mode {
            AddressingMode::Position => {
                let address = self.prog.read_cell(address)?;
                self.prog.write_cell(address, value)
            }
            AddressingMode::Relative => {
                let address = self.relative_base + self.prog.read_cell(address)?;
                self.prog.write_cell(address, value)
            }
            AddressingMode::Invalid | AddressingMode::Immediate => Err(Error::Operation(
                ErrorKind::InvalidAddressingMode,
                Some(opcode.clone()),
            )),
        }
    }

    pub fn run<I>(&mut self, input_iter: &mut I) -> Result<ExecutionResult>
    where
        I: Iterator<Item = i64>,
    {
        loop {
            let opcode = OpCode::from(self.prog.read_cell(self.counter)?);
            if !opcode.is_valid_for_version(self.prog.version) {
                return Err(Error::Operation(ErrorKind::InvalidOpCode, Some(opcode)));
            }
            let instr_len = opcode.instruction_length();

            match opcode.op {
                Op::Add => {
                    let arg1 = self.read_argument(&opcode, 1)?;
                    let arg2 = self.read_argument(&opcode, 2)?;
                    self.write_argument(&opcode, 3, arg1 + arg2)?;
                    self.counter += instr_len;
                }
                Op::Mul => {
                    let arg1 = self.read_argument(&opcode, 1)?;
                    let arg2 = self.read_argument(&opcode, 2)?;
                    self.write_argument(&opcode, 3, arg1 * arg2)?;
                    self.counter += instr_len;
                }
                Op::Input => {
                    if let Some(input_value) = input_iter.next() {
                        self.write_argument(&opcode, 1, input_value)?;
                        self.counter += instr_len;
                    } else {
                        break Ok(ExecutionResult::WaitingForInput);
                    }
                }
                Op::Output => {
                    let arg = self.read_argument(&opcode, 1)?;
                    self.counter += instr_len;
                    break Ok(ExecutionResult::OutputProduced(arg));
                }
                Op::JumpIfTrue => {
                    let arg1 = self.read_argument(&opcode, 1)?;
                    let arg2 = self.read_argument(&opcode, 2)?;
                    if arg1 != 0 {
                        self.counter = arg2;
                    } else {
                        self.counter += instr_len;
                    }
                }
                Op::JumpIfFalse => {
                    let arg1 = self.read_argument(&opcode, 1)?;
                    let arg2 = self.read_argument(&opcode, 2)?;
                    if arg1 == 0 {
                        self.counter = arg2;
                    } else {
                        self.counter += instr_len;
                    }
                }
                Op::LessThan => {
                    let arg1 = self.read_argument(&opcode, 1)?;
                    let arg2 = self.read_argument(&opcode, 2)?;
                    let new_value = if arg1 < arg2 { 1 } else { 0 };
                    self.write_argument(&opcode, 3, new_value)?;
                    self.counter += instr_len;
                }
                Op::Equals => {
                    let arg1 = self.read_argument(&opcode, 1)?;
                    let arg2 = self.read_argument(&opcode, 2)?;
                    let new_value = if arg1 == arg2 { 1 } else { 0 };
                    self.write_argument(&opcode, 3, new_value)?;
                    self.counter += instr_len;
                }
                Op::RelativeBaseOffset => {
                    let arg = self.read_argument(&opcode, 1)?;
                    self.relative_base += arg;
                    self.counter += instr_len;
                }
                Op::Halt => break Ok(ExecutionResult::Halted),
                Op::Invalid => unreachable!(),
            }
        }
    }
}
