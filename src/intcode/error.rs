use std::error;
use std::fmt;

#[derive(Debug)]
pub enum Error {
    Operation(ErrorKind, Option<super::OpCode>),
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Copy, Debug)]
pub enum ErrorKind {
    MemoryAccessOutOfBounds,
    InvalidOpCode,
    InvalidAddressingMode,
}

impl ErrorKind {
    fn as_str(self) -> &'static str {
        match self {
            ErrorKind::MemoryAccessOutOfBounds => "accessed memory out of bounds",
            ErrorKind::InvalidOpCode => "invalid opcode",
            ErrorKind::InvalidAddressingMode => "invalid adressing mode",
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Operation(ref kind, _) => kind.as_str(),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Operation(ref kind, _) => write!(f, "Something went wrong {}", kind.as_str()),
        }
    }
}
