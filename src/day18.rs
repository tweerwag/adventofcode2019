use std::{
    convert::TryInto,
    collections::HashMap,
    io::{BufRead, Write},
};

use crate::{graph, point::Point};

#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
struct BitMap32(u32);

impl BitMap32 {
    fn get(&self, bit: usize) -> bool {
        assert!(bit < 32);
        (self.0 & (1 << bit)) != 0
    }

    fn set(&self, bit: usize) -> BitMap32 {
        assert!(bit < 32);
        BitMap32(self.0 | (1 << bit))
    }

    fn reset(&mut self, bit: usize) {
        assert!(bit < 32);
        self.0 &= !(1 << bit);
    }
}

fn key_value(key: char) -> usize {
    assert!(key.is_ascii_alphabetic());
    (key.to_ascii_lowercase() as usize) - ('a' as usize)
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let map = {
        let mut input_str = String::new();
        input.read_to_string(&mut input_str).unwrap();
        input_str
            .chars()
            .scan(Point { x: 0, y: 0 }, |pos, c| {
                let ret = (*pos, c);
                if c == '\n' {
                    pos.x = 0;
                    pos.y -= 1;
                } else {
                    pos.x += 1;
                }
                Some(ret)
            })
            .filter(|&(_, c)| c != '\n')
            .collect::<HashMap<_, _>>()
    };

    let all_keys = map
        .values()
        .copied()
        .filter(char::is_ascii_lowercase)
        .map(|c| key_value(c))
        .fold(BitMap32(0), |acc, key| acc.set(key));

    //print!("{}", &prog_output.iter().map(|&x| x as u8 as char).collect::<String>());

    match part {
        1 => {
            let start = map
            .iter()
            .find_map(|(&pos, &c)| if c == '@' { Some(pos) } else { None })
            .unwrap();
            let (_, path_len) = graph::dijkstra_by(
                (start, BitMap32(0)),
                |(_, keys)| keys == all_keys,
                |(pos, keys)| {
                    //println!("At {} with keys {}", pos, keys.0);
                    pos.neighbours()
                        .filter_map(|pos| {
                            let c = map.get(&pos).copied().unwrap_or('%');
                            if c == '.'
                                || c == '@'
                                || (c.is_ascii_uppercase() && keys.get(key_value(c)))
                            {
                                Some(((pos, keys), 1))
                            } else if c.is_ascii_lowercase() {
                                Some(((pos, keys.set(key_value(c))), 1))
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<_>>()
                },
            )
            .unwrap();

            writeln!(output, "{}", path_len).unwrap();
        }
        2 => {
            let map = {
                let mut mut_map = map;
                let pos = mut_map.iter().find_map(|(&pos, &c)| if c == '@' { Some(pos) } else { None }).unwrap();
                mut_map.insert(pos, '#');
                mut_map.insert(Point { x: pos.x - 1, ..pos }, '#');
                mut_map.insert(Point { x: pos.x + 1, ..pos }, '#');
                mut_map.insert(Point { y: pos.y - 1, ..pos }, '#');
                mut_map.insert(Point { y: pos.y + 1, ..pos }, '#');
                mut_map.insert(Point { x: pos.x - 1, y: pos.y - 1 }, '@');
                mut_map.insert(Point { x: pos.x + 1, y: pos.y - 1 }, '@');
                mut_map.insert(Point { x: pos.x - 1, y: pos.y + 1 }, '@');
                mut_map.insert(Point { x: pos.x + 1, y: pos.y + 1 }, '@');
                mut_map
            };
            let robots = map.iter().filter_map(|(&pos, &c)| if c == '@' { Some(pos) } else { None }).collect::<Vec<_>>();
            let start: [Point; 4] = (&robots as &[Point]).try_into().expect("not exactly 4 robots");
            let (_, path_len) = graph::dijkstra_by(
                (start, BitMap32(0)),
                |(_, keys)| keys == all_keys,
                |(posses, keys)| {
                    //println!("At {} with keys {}", pos, keys.0);
                    let mut neighbours = vec![];
                    for (i, &pos) in posses.iter().enumerate() {
                        for neighbour in pos.neighbours() {
                            let c = map.get(&pos).copied().unwrap_or('%');
                            if c == '.'
                                || c == '@'
                                || (c.is_ascii_uppercase() && keys.get(key_value(c)))
                            {
                                let new_neighbour = {
                                    let mut x = posses;
                                    x[i] = neighbour;
                                    x
                                };
                                neighbours.push(((new_neighbour, keys), 1));
                            } else if c.is_ascii_lowercase() {
                                let new_neighbour = {
                                    let mut x = posses;
                                    x[i] = neighbour;
                                    x
                                };
                                neighbours.push(((new_neighbour, keys.set(key_value(c))), 1));
                            }
                        }
                    }
                    neighbours
                },
            )
            .unwrap();

            writeln!(output, "{}", path_len).unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn examples_part1() {
        assert_io_eq!(1, read_to_string("test_inputs/day18-1.txt").unwrap(), "8");
        assert_io_eq!(1, read_to_string("test_inputs/day18-2.txt").unwrap(), "86");
        assert_io_eq!(1, read_to_string("test_inputs/day18-3.txt").unwrap(), "132");
        assert_io_eq!(1, read_to_string("test_inputs/day18-4.txt").unwrap(), "136");
        assert_io_eq!(1, read_to_string("test_inputs/day18-5.txt").unwrap(), "81");
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day18.txt").unwrap(), "59645");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day17.txt").unwrap(), "840248");
    }
}
