use std::io::{BufRead, Write};
use std::iter;

use crate::intcode::{Program, Version};
use crate::util;

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut prog = Program::parse(&util::read_line(&mut input), Version::Day09);
    let prog_output = prog.execute_with_input(0, iter::once(part as i64)).unwrap();

    write!(output, "{}", prog_output[0]).unwrap();
    for &x in &prog_output[1..] {
        write!(output, ",{}", x).unwrap();
    }
    writeln!(output).unwrap();
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn examples() {
        assert_io_eq!(
            1,
            "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99",
            "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
        );
        assert_io_eq!(1, "1102,34915192,34915192,7,4,7,99,0", "1219070632396864");
        assert_io_eq!(1, "104,1125899906842624,99", "1125899906842624");
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day09.txt").unwrap(), "2465411646");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day09.txt").unwrap(), "69781");
    }
}
