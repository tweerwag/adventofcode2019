use std::io::{BufRead, Write};

use crate::intcode::{Program, Version};
use crate::util;

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut program = Program::parse(&util::read_line(&mut input), Version::Day02);

    match part {
        1 => {
            program.write_cell(1, 12).unwrap();
            program.write_cell(2, 2).unwrap();
            program.execute(0).unwrap();
            writeln!(output, "{}", program.read_cell(0).unwrap()).unwrap();
        }
        2 => {
            let prog_size = program.size();
            for noun in 0..prog_size {
                for verb in 0..prog_size {
                    let mut program = program.clone();
                    program.write_cell(1, noun as i64).unwrap();
                    program.write_cell(2, verb as i64).unwrap();
                    if let Ok(()) = program.execute(0) {
                        if program.read_cell(0).unwrap() == 19_690_720 {
                            writeln!(output, "{}", 100 * noun + verb).unwrap();
                            return;
                        }
                    }
                }
            }
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use super::*;
    use crate::assert_io_eq;

    macro_rules! assert_prog_eq {
        ($input:expr, $output:expr) => {
            let inp = String::from($input);
            let mut prog = Program::parse(&inp, Version::Day02);
            prog.execute(0).unwrap();
            assert_eq!(prog.get_memory(), $output);
        };
    }

    #[test]
    fn examples() {
        assert_prog_eq!("1,0,0,0,99", &[2, 0, 0, 0, 99]);
        assert_prog_eq!("2,3,0,3,99", &[2, 3, 0, 6, 99]);
        assert_prog_eq!("2,4,4,5,99,0", &[2, 4, 4, 5, 99, 9801]);
        assert_prog_eq!("1,1,1,4,99,5,6,0,99", &[30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day02.txt").unwrap(), "4714701");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day02.txt").unwrap(), "5121");
    }
}
