use std::io::{BufRead, Write};

use crate::{graph, point::Point};

#[derive(Debug)]
struct Maze {
    nodes: Vec<(Point, Option<String>)>,
    height: i64,
    width: i64,
}

impl Maze {
    fn read_from_input(mut input: impl BufRead) -> Maze {
        let grid = {
            let mut v = vec![];
            loop {
                let mut buf = String::new();
                if input.read_line(&mut buf).unwrap() == 0 {
                    break v;
                }
                v.push(buf.chars().collect::<Vec<_>>());
            }
        };
        let mut maze = vec![];

        for y in 0..grid.len() {
            let line = &grid[y];
            for x in 0..line.len() {
                if line[x] == '.' {
                    let label = if x >= 2
                        && line[x - 2].is_ascii_uppercase()
                        && line[x - 1].is_ascii_uppercase()
                    {
                        let mut s = String::new();
                        s.extend(&[line[x - 2], line[x - 1]]);
                        Some(s)
                    } else if x < line.len() - 2
                        && line[x + 1].is_ascii_uppercase()
                        && line[x + 2].is_ascii_uppercase()
                    {
                        let mut s = String::new();
                        s.extend(&[line[x + 1], line[x + 2]]);
                        Some(s)
                    } else if y >= 2
                        && grid[y - 2][x].is_ascii_uppercase()
                        && grid[y - 1][x].is_ascii_uppercase()
                    {
                        let mut s = String::new();
                        s.extend(&[grid[y - 2][x], grid[y - 1][x]]);
                        Some(s)
                    } else if y < grid.len() - 2
                        && grid[y + 1][x].is_ascii_uppercase()
                        && grid[y + 2][x].is_ascii_uppercase()
                    {
                        let mut s = String::new();
                        s.extend(&[grid[y + 1][x], grid[y + 2][x]]);
                        Some(s)
                    } else {
                        None
                    };
                    maze.push((
                        Point {
                            x: x as i64,
                            y: y as i64,
                        },
                        label,
                    ));
                }
            }
        }

        Maze {
            nodes: maze,
            height: grid.len() as i64,
            width: grid[0].len() as i64,
        }
    }

    fn get_position_of_label(&self, label: &str) -> Option<Point> {
        self.nodes
            .iter()
            .filter(|&(_, other_label)| other_label.as_ref().map(String::as_str) == Some(label))
            .map(|&(pos, _)| pos)
            .next()
    }

    fn is_outside_connection(&self, node: Point) -> bool {
        node.x == 2 || node.x == self.width - 4 || node.y == 2 || node.y == self.height - 3
    }

    fn get_connections(&self) -> Vec<(Point, Point)> {
        let mut labels = self
            .nodes
            .iter()
            .filter_map(|(_, o)| o.as_ref())
            .map(String::as_str)
            .filter(|&label| label != "AA" && label != "ZZ")
            .collect::<Vec<_>>();
        labels.sort();
        labels.dedup();

        let mut connections = vec![];
        for &label in &labels {
            let mut iter = self
                .nodes
                .iter()
                .filter(|&(_, other_label)| other_label.as_ref().map(String::as_str) == Some(label))
                .map(|&(pos, _)| pos);
            let first = iter.next().unwrap();
            let second = iter.next().unwrap();

            if self.is_outside_connection(first) {
                connections.push((first, second));
            //println!("Label {}: {} -- {}", label, first, second);
            } else {
                connections.push((second, first));
                //println!("Label {}: {} -- {}", label, second, first);
            }
        }
        connections
    }

    fn get_normal_graph(&self) -> graph::Graph<Point> {
        let mut edges = vec![];

        // First add the normal edges
        for &(point, _) in &self.nodes {
            for other in point.neighbours() {
                if self.nodes.iter().any(|&(n, _)| n == other) {
                    edges.push((point, other));
                }
            }
        }

        // Look up all labels
        self.nodes
            .iter()
            .filter_map(|&(_, ref label)| label.as_ref().map(String::as_str))
            .filter(|&label| label != "AA" && label != "ZZ")
            .for_each(|label| {
                let mut iter = self
                    .nodes
                    .iter()
                    .filter(|&(_, ref this_label)| {
                        this_label.as_ref().map(String::as_str) == Some(label)
                    })
                    .map(|&(pos, _)| pos);
                let node1 = iter.next().unwrap();
                let node2 = iter.next().expect("Broken portal");
                edges.push((node1, node2));
                edges.push((node2, node1));
            });

        graph::Graph::from_edges(edges)
    }

    fn get_recursive_graph(&self, max_level: usize) -> graph::Graph<(usize, Point)> {
        let mut edges = vec![];

        let mut inside_edges = vec![];
        for &(point, _) in &self.nodes {
            for other in point.neighbours() {
                if self.nodes.iter().any(|&(n, _)| n == other) {
                    inside_edges.push((point, other));
                }
            }
        }

        let connections = self.get_connections();

        for level in 0..=max_level {
            if level > 0 {
                // Stitch up the connections
                for &(outside, inside) in &connections {
                    let inside = (level - 1, inside);
                    let outside = (level, outside);
                    edges.push((inside, outside));
                    edges.push((outside, inside));
                }
            }

            // Add the normal edges
            edges.extend(
                inside_edges
                    .iter()
                    .copied()
                    .map(|(n1, n2)| ((level, n1), (level, n2))),
            );
        }

        graph::Graph::from_edges(edges)
    }
}

pub fn execute(part: i32, input: impl BufRead, mut output: impl Write) {
    let maze = Maze::read_from_input(input);

    match part {
        1 => {
            let graph = maze.get_normal_graph();
            let start = maze.get_position_of_label("AA").unwrap();
            let goal = maze.get_position_of_label("ZZ").unwrap();

            let (_, path_len) = graph::dijkstra(&graph, start, &[goal]).expect("No path found");
            writeln!(output, "{}", path_len).unwrap();
        }
        2 => {
            let start = (0, maze.get_position_of_label("AA").unwrap());
            let goal = (0, maze.get_position_of_label("ZZ").unwrap());
            let connections = maze.get_connections();

            let mut max_level = 0;
            let path_len = loop {
                let graph = maze.get_recursive_graph(max_level);
                let mut goals = connections
                    .iter()
                    .copied()
                    .map(|(_, n)| (max_level, n))
                    .collect::<Vec<_>>();
                goals.push(goal);
                match graph::dijkstra(&graph, start, &goals) {
                    Some((n, path_len)) if n == goal => break path_len,
                    _ => max_level = (max_level + 1) * 2 - 1,
                }
            };
            writeln!(output, "{}", path_len).unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn examples_part1() {
        assert_io_eq!(1, read_to_string("test_inputs/day20-1.txt").unwrap(), "23");
        assert_io_eq!(1, read_to_string("test_inputs/day20-2.txt").unwrap(), "58");
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day20.txt").unwrap(), "496");
    }

    #[test]
    fn examples_part2() {
        assert_io_eq!(2, read_to_string("test_inputs/day20-1.txt").unwrap(), "26");
        assert_io_eq!(2, read_to_string("test_inputs/day20-3.txt").unwrap(), "396");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day20.txt").unwrap(), "5886");
    }
}
