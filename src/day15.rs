use std::collections::HashMap;
use std::io::{BufRead, Write};
use std::iter;

use crate::graph;
use crate::intcode::{ExecutionResult, Program, Version};
use crate::point::{Direction, Point};
use crate::util;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum RoomTile {
    Empty,
    Wall,
    OxygenSystem,
}

type Room = HashMap<Point, RoomTile>;

// fn display_room(room: &Room) {
//     let &min_x = room.keys().map(|Point { x, .. }| x).min().unwrap();
//     let &max_x = room.keys().map(|Point { x, .. }| x).max().unwrap();
//     let &min_y = room.keys().map(|Point { y, .. }| y).min().unwrap();
//     let &max_y = room.keys().map(|Point { y, .. }| y).max().unwrap();
//     for y in (min_y..=max_y).rev() {
//         for x in min_x..=max_x {
//             let c = match room.get(&Point { x, y }).copied() {
//                 Some(RoomTile::Empty) => '.',
//                 Some(RoomTile::Wall) => '#',
//                 Some(RoomTile::OxygenSystem) => '%',
//                 None => '?',
//             };
//             print!("{}", c);
//         }
//         println!();
//     }
// }

fn dir_to_nswe(direction: Direction) -> i64 {
    match direction {
        Direction::Right => 4,
        Direction::Up => 1,
        Direction::Left => 3,
        Direction::Down => 2,
    }
}

fn next_dir(direction: Direction) -> Option<Direction> {
    match direction {
        Direction::Right => Some(Direction::Up),
        Direction::Up => Some(Direction::Left),
        Direction::Left => Some(Direction::Down),
        Direction::Down => None,
    }
}

fn explore_room(prog: &mut Program) -> Room {
    let mut executor = prog.executor(0);

    let mut room = Room::new();
    let mut current_position = Point { x: 0, y: 0 };
    let mut path = vec![Direction::Right];

    room.insert(current_position, RoomTile::Empty); // Educated guess...

    while !path.is_empty() {
        let direction = path.last().copied().unwrap();
        let new_position = current_position.move_forward(direction);
        let mut do_backtrack = true;

        room.entry(new_position).or_insert_with(|| {
            let status = match executor
                .run(&mut iter::once(dir_to_nswe(direction)))
                .unwrap()
            {
                ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
                ExecutionResult::OutputProduced(status) => status,
                ExecutionResult::Halted => panic!("Unexpected halt"),
            };

            match status {
                0 => RoomTile::Wall,
                1 => {
                    current_position = new_position;
                    path.push(Direction::Right);
                    do_backtrack = false;
                    RoomTile::Empty
                }
                2 => {
                    current_position = new_position;
                    path.push(Direction::Right);
                    do_backtrack = false;
                    RoomTile::OxygenSystem
                }
                _ => {
                    panic!("Unexpected status byte {}", status);
                }
            }
        });

        if do_backtrack {
            'backtrack: while let Some(direction) = path.pop() {
                if let Some(new_dir) = next_dir(direction) {
                    path.push(new_dir);
                    break 'backtrack;
                } else if let Some(prev_dir) = path.last().copied() {
                    match executor
                        .run(&mut iter::once(dir_to_nswe(-prev_dir)))
                        .unwrap()
                    {
                        ExecutionResult::WaitingForInput => panic!("Unexpected wait for input"),
                        ExecutionResult::OutputProduced(_) => {}
                        ExecutionResult::Halted => panic!("Unexpected halt"),
                    }
                    current_position = current_position.move_backward(prev_dir);
                }
            }
        }
    }

    room
}

impl From<&Room> for graph::Graph<Point> {
    fn from(room: &Room) -> graph::Graph<Point> {
        let edges = room
            .iter()
            .filter(|&(_, &room_tile)| room_tile != RoomTile::Wall)
            .map(|(&pos, _)| pos)
            .map(|pos| {
                pos.neighbours()
                    .filter(|other| room.contains_key(other))
                    .map(move |other| (pos, other))
            })
            .flatten();
        graph::Graph::from_edges(edges)
    }
}

fn find_shortest_path(room: &Room) -> usize {
    let start = Point { x: 0, y: 0 };
    let (&goal, _) = room
        .iter()
        .find(|&(_, &room_tile)| room_tile == RoomTile::OxygenSystem)
        .expect("No goal position found");
    let (_, cost) = graph::dijkstra(&room.into(), start, &[goal]).expect("No path found");
    cost
}

fn find_longest_path(room: &Room) -> usize {
    let (&start, _) = room
        .iter()
        .find(|&(_, &room_tile)| room_tile == RoomTile::OxygenSystem)
        .expect("No goal position found");
    let (_, cost) = graph::dijkstra_old(&room.into(), start, &[]).expect("No path found");
    cost
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut prog = Program::parse(&util::read_line(&mut input), Version::Day09);
    let room = explore_room(&mut prog);

    match part {
        1 => writeln!(output, "{}", find_shortest_path(&room)).unwrap(),
        2 => writeln!(output, "{}", find_longest_path(&room)).unwrap(),
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day15.txt").unwrap(), "204");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day15.txt").unwrap(), "340");
    }
}
