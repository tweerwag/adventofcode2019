use std::cmp::Ordering;
use std::ops;

use crate::util;

type Int = i64;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Rational {
    num: Int,
    den: Int,
}

impl From<Int> for Rational {
    fn from(x: Int) -> Rational {
        Rational { num: x, den: 1 }
    }
}

impl PartialOrd for Rational {
    fn partial_cmp(&self, other: &Rational) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Rational {
    fn cmp(&self, other: &Rational) -> Ordering {
        let common_den = util::lcm(self.den as usize, other.den as usize) as Int;
        let left_num = self.num * (common_den / self.den);
        let right_num = other.num * (common_den / other.den);
        left_num.cmp(&right_num)
    }
}

impl Rational {
    fn normalize(mut num: Int, mut den: Int) -> Rational {
        let gcd = util::gcd(num.abs() as usize, den as usize) as Int;
        if den < 0 {
            num = -num;
            den = -den;
        }
        Rational {
            num: num / gcd,
            den: den / gcd,
        }
    }
}

// Ops impls with actual logic

impl ops::Add for Rational {
    type Output = Rational;

    fn add(self, rhs: Rational) -> Rational {
        let new_den = util::lcm(self.den as usize, rhs.den as usize) as Int;
        let new_num = self.num * (new_den / self.den) + rhs.num * (new_den / rhs.den);
        Rational::normalize(new_num, new_den)
    }
}

impl ops::Neg for Rational {
    type Output = Rational;

    fn neg(self) -> Rational {
        Rational {
            num: -self.num,
            den: self.den,
        }
    }
}

impl ops::Mul for Rational {
    type Output = Rational;

    fn mul(self, rhs: Rational) -> Rational {
        Rational::normalize(self.num * rhs.num, self.den * rhs.den)
    }
}

impl ops::Div for Rational {
    type Output = Rational;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, rhs: Rational) -> Rational {
        if rhs.num == 0 {
            panic!("Division by zero");
        }
        Rational::normalize(self.num * rhs.den, self.den * rhs.num)
    }
}

// Ops impls implemented with above ops

impl ops::AddAssign for Rational {
    fn add_assign(&mut self, rhs: Rational) {
        *self = *self + rhs;
    }
}

impl ops::Sub for Rational {
    type Output = Rational;

    fn sub(self, rhs: Rational) -> Rational {
        self + -rhs
    }
}

impl ops::SubAssign for Rational {
    fn sub_assign(&mut self, rhs: Rational) {
        *self = *self - rhs;
    }
}

impl ops::MulAssign for Rational {
    fn mul_assign(&mut self, rhs: Rational) {
        *self = *self * rhs;
    }
}

impl ops::DivAssign for Rational {
    fn div_assign(&mut self, rhs: Rational) {
        *self = *self / rhs;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(
            Rational { num: 1, den: 1 } + Rational { num: 1, den: 1 },
            Rational { num: 2, den: 1 }
        );
        assert_eq!(
            Rational { num: 2, den: 3 } + Rational { num: 1, den: 4 },
            Rational { num: 11, den: 12 }
        );
        assert_eq!(
            Rational { num: -2, den: 3 } + Rational { num: 1, den: 4 },
            Rational { num: -5, den: 12 }
        );
        assert_eq!(
            Rational { num: -2, den: 3 } + Rational { num: 2, den: 3 },
            Rational { num: 0, den: 1 }
        );
        assert_eq!(
            Rational { num: 0, den: 1 } + Rational { num: 2, den: 3 },
            Rational { num: 2, den: 3 }
        );
        assert_eq!(
            Rational { num: 0, den: 1 } + Rational { num: 0, den: 1 },
            Rational { num: 0, den: 1 }
        );
    }

    #[test]
    fn test_sub() {
        assert_eq!(
            Rational { num: 1, den: 3 } - Rational { num: 1, den: 4 },
            Rational { num: 1, den: 12 }
        );
    }

    #[test]
    fn test_mul() {
        assert_eq!(
            Rational { num: 2, den: 3 } * Rational { num: 3, den: 2 },
            Rational { num: 1, den: 1 }
        );
    }

    #[test]
    fn test_div() {
        assert_eq!(
            Rational { num: 2, den: 3 } / Rational { num: 3, den: 2 },
            Rational { num: 4, den: 9 }
        );
        assert_eq!(
            Rational { num: -2, den: 3 } / Rational { num: 3, den: 2 },
            Rational { num: -4, den: 9 }
        );
        assert_eq!(
            Rational { num: 2, den: 3 } / Rational { num: -3, den: 2 },
            Rational { num: -4, den: 9 }
        );
        assert_eq!(
            Rational { num: -2, den: 3 } / Rational { num: -3, den: 2 },
            Rational { num: 4, den: 9 }
        );
    }

    #[test]
    fn test_from() {
        assert_eq!(
            Rational::from(2) / Rational::from(3),
            Rational { num: 2, den: 3 }
        );
    }

    #[test]
    fn test_ord() {
        assert!(Rational { num: 1, den: 3 } > Rational { num: 1, den: 4 });
        assert!(Rational { num: 12, den: 13 } > Rational { num: 11, den: 12 });
        assert!(Rational { num: -12, den: 13 } < Rational { num: 11, den: 12 });
        assert!(Rational { num: -12, den: 13 } < Rational { num: -11, den: 12 });
    }
}
