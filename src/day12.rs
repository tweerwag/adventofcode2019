use crate::point3;
use crate::util;
use regex::Regex;
use std::io::{BufRead, Write};

type Point3 = point3::Point3<i64>;

#[derive(Clone, PartialEq, Eq)]
struct State {
    pos: Vec<i64>,
    vel: Vec<i64>,
}

impl State {
    fn new(pos: &[i64]) -> State {
        State {
            pos: pos.to_owned(),
            vel: pos.iter().map(|_| 0).collect(),
        }
    }
}

struct Simulator {
    state: State,
}

impl Iterator for Simulator {
    type Item = State;

    fn next(&mut self) -> Option<State> {
        let ret = self.state.clone();

        let n = self.state.pos.len();
        let pos = &mut self.state.pos;
        let vel = &mut self.state.vel;

        for i in 0..n {
            for j in (i + 1)..n {
                let diff = pos[j] - pos[i];
                vel[i] += diff.signum();
                vel[j] -= diff.signum();
            }
        }

        for (pos, vel) in pos.iter_mut().zip(vel) {
            *pos += *vel;
        }

        Some(ret)
    }
}

fn parse_points(s: &str) -> Vec<Point3> {
    let re = Regex::new(r"<x=(?P<x>-?\d+),\s*y=(?P<y>-?\d+),\s*z=(?P<z>-?\d+)>").unwrap();
    re.captures_iter(s)
        .map(|cap| Point3 {
            x: cap["x"].parse().unwrap(),
            y: cap["y"].parse().unwrap(),
            z: cap["z"].parse().unwrap(),
        })
        .collect()
}

fn simulate_part1(mut input: impl BufRead, number_of_steps: usize) -> i64 {
    let pos = {
        let mut buf = String::new();
        input.read_to_string(&mut buf).unwrap();
        parse_points(&buf)
    };
    let simulate_coord = |coords: &[i64]| {
        let mut simulator = Simulator {
            state: State::new(coords),
        };
        simulator.nth(number_of_steps).unwrap()
    };
    let coord_x = simulate_coord(&pos.iter().map(|p| p.x).collect::<Vec<_>>());
    let coord_y = simulate_coord(&pos.iter().map(|p| p.y).collect::<Vec<_>>());
    let coord_z = simulate_coord(&pos.iter().map(|p| p.z).collect::<Vec<_>>());

    (0..pos.len())
        .map(|i| {
            (
                Point3 {
                    x: coord_x.pos[i],
                    y: coord_y.pos[i],
                    z: coord_z.pos[i],
                },
                Point3 {
                    x: coord_x.vel[i],
                    y: coord_y.vel[i],
                    z: coord_z.vel[i],
                },
            )
        })
        .map(|(pos, vel)| pos.manhattan_norm() * vel.manhattan_norm())
        .sum()
}

fn do_part2(mut input: impl BufRead) -> usize {
    let pos = {
        let mut buf = String::new();
        input.read_to_string(&mut buf).unwrap();
        parse_points(&buf)
    };

    let (mu_x, lam_x) = cycle_detect(&pos.iter().map(|p| p.x).collect::<Vec<_>>());
    let (mu_y, lam_y) = cycle_detect(&pos.iter().map(|p| p.y).collect::<Vec<_>>());
    let (mu_z, lam_z) = cycle_detect(&pos.iter().map(|p| p.z).collect::<Vec<_>>());

    assert_eq!(mu_x, 0);
    assert_eq!(mu_y, 0);
    assert_eq!(mu_z, 0);

    util::lcm(util::lcm(lam_x, lam_y), lam_z)
}

fn cycle_detect(coords: &[i64]) -> (usize, usize) {
    let initial_state = State::new(coords);

    // Floyd's trick follows
    let mut tortoise = Simulator {
        state: initial_state.clone(),
    };
    let mut hare = Simulator {
        state: initial_state.clone(),
    };

    tortoise.next();
    hare.next();
    hare.next();
    while hare.state != tortoise.state {
        tortoise.next();
        hare.next();
        hare.next();
    }

    let mut mu = 0;
    tortoise = Simulator {
        state: initial_state,
    };
    while hare.state != tortoise.state {
        tortoise.next();
        hare.next();
        mu += 1;
    }

    let mut lambda = 1;
    tortoise.next();
    while hare.state != tortoise.state {
        tortoise.next();
        lambda += 1;
    }

    (mu, lambda)
}

pub fn execute(part: i32, input: impl BufRead, mut output: impl Write) {
    match part {
        1 => {
            let total_energy = simulate_part1(input, 1000);
            writeln!(output, "{}", total_energy).unwrap();
        }
        2 => {
            let first_rep = do_part2(input);
            writeln!(output, "{}", first_rep).unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn examples_part1() {
        assert_eq!(
            simulate_part1(
                "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>"
                    .as_bytes(),
                10
            ),
            179
        );
        assert_eq!(
            simulate_part1(
                "<x=-8, y=-10, z=0>\n<x=5, y=5, z=10>\n<x=2, y=-7, z=3>\n<x=9, y=-8, z=-3>"
                    .as_bytes(),
                100
            ),
            1940
        );
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day12.txt").unwrap(), "9441");
    }

    #[test]
    fn examples_part2() {
        assert_eq!(
            do_part2(
                "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>"
                    .as_bytes()
            ),
            2772
        );
        assert_eq!(
            do_part2(
                "<x=-8, y=-10, z=0>\n<x=5, y=5, z=10>\n<x=2, y=-7, z=3>\n<x=9, y=-8, z=-3>"
                    .as_bytes()
            ),
            4686774924
        );
    }

    #[test]
    fn part2() {
        assert_io_eq!(
            2,
            read_to_string("inputs/day12.txt").unwrap(),
            "503560201099704"
        );
    }
}
