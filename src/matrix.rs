use std::{error,fmt,iter,ops};

type Num = i64;

#[derive(Debug)]
pub enum MatrixError {
    OutOfBounds{row: usize, column: usize},
}

impl fmt::Display for MatrixError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            MatrixError::OutOfBounds{row, column} => write!(f, "tried to access out of bounds entry at row {}, column {}", row, column),
        }
    }
}

impl error::Error for MatrixError { }

#[derive(Debug,Clone, PartialEq,Eq)]
pub struct Matrix {
    entries: Vec<Num>,
    rows: usize,
    columns: usize,
}

impl Matrix {
    pub fn get_entry(&self, row: usize, column: usize) -> Result<Num, MatrixError> {
        if row < self.rows && column < self.columns {
            Ok(self.entries[row * self.columns + column])
        } else {
            Err(MatrixError::OutOfBounds { row, column })
        }
    }

    pub fn row_vector(entries: &[i64]) -> Matrix {
        Matrix {
            rows: 1,
            columns: entries.len(),
            entries: entries.to_owned(),
        }
    }

    pub fn column_vector(entries: &[i64]) -> Matrix {
        Matrix {
            rows: entries.len(),
            columns: 1,
            entries: entries.to_owned(),
        }
    }

    pub fn from_row_major_order(columns: usize, entries: &[i64]) -> Matrix {
        assert!(entries.len() % columns == 0);
        Matrix {
            rows: entries.len() / columns,
            columns: columns,
            entries: entries.to_owned(),
        }
    }

    pub fn entries(&self) -> &[Num] {
        &self.entries
    }

    pub fn mut_entries(&mut self) -> &mut [Num] {
        &mut self.entries
    }

    pub fn identity(size: usize) -> Matrix {
        let mut entries = vec![0; size * size];
        for i in 0..size {
            entries[i * size + i] = 1;
        }
        Matrix {
            rows: size,
            columns: size,
            entries: entries,
        }
    }
}

impl ops::Mul for Matrix {
    type Output = Matrix;

    fn mul(self, rhs: Matrix) -> Matrix {
        assert_eq!(self.columns, rhs.rows);

        let mut result = Matrix {
            rows: self.rows,
            columns: rhs.columns,
            entries: vec![0; self.rows * rhs.columns],
        };

        for i in 0..result.rows {
            for j in 0..result.columns {
                for k in 0..self.columns {
                    result.entries[i * result.columns + j] += self.entries[i * self.columns + k] * rhs.entries[k * rhs.columns + j];
                }
            }
        }

        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn matrix_multiplication() {
        assert_eq!(Matrix { rows: 2, columns: 2, entries: vec![1, 2, 3, 4] } * Matrix { rows: 2, columns: 2, entries: vec![4, 3, 2, 1] },
            Matrix { rows: 2, columns: 2, entries: vec![8, 5, 20, 13] });
        assert_eq!(Matrix { rows: 2, columns: 3, entries: vec![1; 6] } * Matrix { rows: 3, columns: 4, entries: vec![1; 12] },
            Matrix { rows: 2, columns: 4, entries: vec![3; 8] });
    }
}
