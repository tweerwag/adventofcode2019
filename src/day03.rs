use std::io::{BufRead, Write};
use std::iter;
use std::ops;
use std::slice;

use crate::util;

#[derive(Clone, Copy, Debug)]
struct Point(i32, i32);

impl Point {
    fn manhattan_norm(self) -> i32 {
        self.0.abs() + self.1.abs()
    }
}

impl ops::Sub for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point(self.0 - other.0, self.1 - other.1)
    }
}

#[derive(Clone, Copy, Debug)]
struct Line(Point, Point);

#[derive(PartialEq, Eq)]
enum Orientation {
    Vertical,
    Horizontal,
    Other,
}

impl Line {
    fn orientation(&self) -> Orientation {
        if self.0 .0 == self.1 .0 {
            Orientation::Vertical
        } else if self.0 .1 == self.1 .1 {
            Orientation::Horizontal
        } else {
            Orientation::Other
        }
    }

    fn manhattan_length(&self) -> i32 {
        (self.0 - self.1).manhattan_norm()
    }

    fn contains(&self, p: Point) -> bool {
        if self.orientation() == Orientation::Horizontal {
            let left_x = i32::min(self.0 .0, self.1 .0);
            let right_x = i32::max(self.0 .0, self.1 .0);
            self.0 .1 == p.1 && left_x <= p.0 && p.0 <= right_x
        } else if self.orientation() == Orientation::Vertical {
            let bottom_y = i32::min(self.0 .1, self.1 .1);
            let top_y = i32::max(self.0 .1, self.1 .1);
            self.0 .0 == p.0 && bottom_y <= p.1 && p.1 <= top_y
        } else {
            false
        }
    }

    fn intersection_with(&self, other: &Line) -> Option<Point> {
        let horz;
        let vert;
        if self.orientation() == Orientation::Horizontal
            && other.orientation() == Orientation::Vertical
        {
            horz = self;
            vert = other;
        } else if self.orientation() == Orientation::Vertical
            && other.orientation() == Orientation::Horizontal
        {
            horz = other;
            vert = self;
        } else {
            return None;
        }

        let left_x = i32::min(horz.0 .0, horz.1 .0);
        let right_x = i32::max(horz.0 .0, horz.1 .0);
        let bottom_y = i32::min(vert.0 .1, vert.1 .1);
        let top_y = i32::max(vert.0 .1, vert.1 .1);
        if left_x <= vert.0 .0
            && vert.0 .0 <= right_x
            && bottom_y <= horz.0 .1
            && horz.0 .1 <= top_y
        {
            Some(Point(vert.0 .0, horz.0 .1))
        } else {
            None
        }
    }
}

#[derive(Clone, Debug)]
struct Wire(Vec<Point>);

impl Wire {
    fn parse(s: &str) -> Wire {
        Wire(
            iter::once(Point(0, 0))
                .chain(
                    s.split(',')
                        .map(|x| (x.chars().nth(0).unwrap(), x[1..].parse::<i32>().unwrap()))
                        .scan(Point(0, 0), |cur_point, (dir, dist)| {
                            match dir {
                                'R' => cur_point.0 += dist,
                                'U' => cur_point.1 += dist,
                                'L' => cur_point.0 -= dist,
                                'D' => cur_point.1 -= dist,
                                _ => panic!("Invalid direction"),
                            }
                            Some(*cur_point)
                        }),
                )
                .collect(),
        )
    }

    fn lines(&self) -> Lines {
        Lines {
            iter: self.0.windows(2),
        }
    }

    fn intersections_with(&self, other: &Wire) -> Vec<Point> {
        let mut intersections = vec![];
        for line1 in self.lines() {
            for line2 in other.lines() {
                if let Some(p) = line1.intersection_with(&line2) {
                    intersections.push(p);
                }
            }
        }
        intersections
    }

    fn distance_to(&self, p: Point) -> Option<i32> {
        let mut distance = 0;
        let mut found = false;
        for line in self.lines() {
            if line.contains(p) {
                distance += (p - line.0).manhattan_norm();
                found = true;
                break;
            } else {
                distance += line.manhattan_length();
            }
        }
        if found {
            Some(distance)
        } else {
            None
        }
    }
}

struct Lines<'a> {
    iter: slice::Windows<'a, Point>,
}

impl<'a> Iterator for Lines<'a> {
    type Item = Line;

    fn next(&mut self) -> Option<Line> {
        self.iter.next().map(|x| match *x {
            [p, q] => Line(p, q),
            _ => panic!("Invalid iterator"),
        })
    }
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let wire1 = Wire::parse(&util::read_line(&mut input));
    let wire2 = Wire::parse(&util::read_line(&mut input));
    let intersections = wire1.intersections_with(&wire2);

    match part {
        1 => writeln!(
            output,
            "{}",
            intersections
                .iter()
                .map(|p| p.manhattan_norm())
                .filter(|&x| x > 0)
                .min()
                .unwrap()
        )
        .unwrap(),

        2 => writeln!(
            output,
            "{}",
            intersections
                .iter()
                .map(|&p| wire1.distance_to(p).unwrap() + wire2.distance_to(p).unwrap())
                .filter(|&x| x > 0)
                .min()
                .unwrap()
        )
        .unwrap(),
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn examples_part1() {
        assert_io_eq!(
            1,
            "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83",
            "159"
        );
        assert_io_eq!(
            1,
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            "135"
        );
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day03.txt").unwrap(), "709");
    }

    #[test]
    fn examples_part2() {
        assert_io_eq!(
            2,
            "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83",
            "610"
        );
        assert_io_eq!(
            2,
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            "410"
        );
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day03.txt").unwrap(), "13836");
    }
}
