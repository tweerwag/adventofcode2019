use std::collections::BTreeSet;
use std::io::{BufRead, Write};
use std::ops;

use crate::rational::Rational;

#[derive(PartialOrd, Ord, PartialEq, Eq, Debug, Copy, Clone)]
struct Point(i64, i64);

impl ops::Add for Point {
    type Output = Point;

    fn add(self, rhs: Point) -> Point {
        Point(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl ops::AddAssign for Point {
    fn add_assign(&mut self, rhs: Point) {
        self.0 += rhs.0;
        self.1 += rhs.1;
    }
}

impl ops::Sub for Point {
    type Output = Point;

    fn sub(self, rhs: Point) -> Point {
        Point(self.0 - rhs.0, self.1 - rhs.1)
    }
}

impl Point {
    fn manhattan_norm(self) -> i64 {
        self.0.abs() + self.1.abs()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
struct GridAngle {
    /// Quadrant 0: from (0, -1) to (1, 0) (not including)
    /// Quadrant 1: from (1, 0) to (0, 1)
    /// Quadrant 2: from (0, 1) to (-1, 0)
    /// Quadrant 4: from (-1, 0) to (0, -1)
    quadrant: u8,
    /// For quadrant 0 and 2, this is x / y
    /// For quadrant 1 and 3, this is y / x
    rational: Rational,
}

impl From<Point> for GridAngle {
    fn from(p: Point) -> GridAngle {
        let Point(x, y) = p;

        if x >= 0 && y < 0 {
            GridAngle {
                quadrant: 0,
                rational: Rational::from(x) / Rational::from(-y),
            }
        } else if x > 0 && y >= 0 {
            GridAngle {
                quadrant: 1,
                rational: Rational::from(y) / Rational::from(x),
            }
        } else if x <= 0 && y > 0 {
            GridAngle {
                quadrant: 2,
                rational: Rational::from(-x) / Rational::from(y),
            }
        } else if x < 0 && y <= 0 {
            GridAngle {
                quadrant: 3,
                rational: Rational::from(-y) / Rational::from(-x),
            }
        } else {
            panic!("Trying to convert zero point to angle");
        }
    }
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let mut asteroids = BTreeSet::new();
    let mut buf = String::new();
    let mut line_num = 0;

    while input.read_line(&mut buf).unwrap() > 0 {
        for (x, c) in buf.trim().chars().enumerate() {
            match c {
                '.' => {}
                '#' => {
                    asteroids.insert(Point(x as i64, line_num as i64));
                }
                _ => {
                    panic!("Unexpected character encountered");
                }
            }
        }

        buf.clear();
        line_num += 1;
    }

    let (base_asteroid, max_visible) = asteroids
        .iter()
        .map(|&cur_asteroid| {
            let mut angles = asteroids
                .iter()
                .filter(|&&other| other != cur_asteroid)
                .map(|&other_asteroid| other_asteroid - cur_asteroid)
                .map(GridAngle::from)
                .collect::<Vec<_>>();
            angles.sort_unstable();
            angles.dedup();
            (cur_asteroid, angles.len())
        })
        .max_by_key(|&(_, x)| x)
        .unwrap();

    match part {
        1 => {
            writeln!(output, "{}", max_visible).unwrap();
        }
        2 => {
            let mut angles = asteroids
                .iter()
                .filter(|&&other| other != base_asteroid)
                .map(|&other_asteroid| {
                    (
                        GridAngle::from(other_asteroid - base_asteroid),
                        other_asteroid,
                    )
                })
                .collect::<Vec<_>>();
            angles
                .sort_unstable_by_key(|&(angle, p)| (angle, (p - base_asteroid).manhattan_norm()));
            assert!(angles.len() >= 200);

            let mut cur_angle = angles[0].0;
            angles.remove(0);

            for i in 1..200 {
                let position = angles.iter().position(|&(a, _)| a > cur_angle).unwrap_or(0);
                if i == 199 {
                    let Point(x, y) = angles[position].1;
                    writeln!(output, "{}", x * 100 + y).unwrap();
                }
                cur_angle = angles[position].0;
                angles.remove(position);
            }
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn examples_part1() {
        assert_io_eq!(
            1,
            ".#..#
.....
#####
....#
...##",
            "8"
        );
        assert_io_eq!(
            1,
            "......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####",
            "33"
        );
        assert_io_eq!(
            1,
            "#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.",
            "35"
        );
        assert_io_eq!(
            1,
            ".#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..",
            "41"
        );
        assert_io_eq!(
            1,
            ".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##",
            "210"
        );
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day10.txt").unwrap(), "329");
    }

    #[test]
    fn example_part2() {
        assert_io_eq!(
            2,
            ".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##",
            "802"
        );
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day10.txt").unwrap(), "512");
    }
}
