use std::{fmt, ops};

#[derive(Debug, Hash, PartialEq, Eq, Copy, Clone)]
pub struct Point {
    pub x: i64,
    pub y: i64,
}

impl Point {
    pub fn move_forward(self, dir: Direction) -> Point {
        match dir {
            Direction::Right => Point {
                x: self.x + 1,
                ..self
            },
            Direction::Up => Point {
                y: self.y + 1,
                ..self
            },
            Direction::Left => Point {
                x: self.x - 1,
                ..self
            },
            Direction::Down => Point {
                y: self.y - 1,
                ..self
            },
        }
    }

    pub fn move_backward(self, dir: Direction) -> Point {
        self.move_forward(-dir)
    }

    pub fn neighbours(self) -> impl Iterator<Item = Point> {
        vec![
            Direction::Right,
            Direction::Up,
            Direction::Left,
            Direction::Down,
        ]
        .into_iter()
        .map(move |dir| self.move_forward(dir))
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Direction {
    Right,
    Up,
    Left,
    Down,
}

impl Direction {
    pub fn turn_right(self) -> Direction {
        match self {
            Direction::Right => Direction::Down,
            Direction::Up => Direction::Right,
            Direction::Left => Direction::Up,
            Direction::Down => Direction::Left,
        }
    }

    pub fn turn_left(self) -> Direction {
        match self {
            Direction::Right => Direction::Up,
            Direction::Up => Direction::Left,
            Direction::Left => Direction::Down,
            Direction::Down => Direction::Right,
        }
    }
}

impl ops::Neg for Direction {
    type Output = Direction;

    fn neg(self) -> Direction {
        match self {
            Direction::Right => Direction::Left,
            Direction::Up => Direction::Down,
            Direction::Left => Direction::Right,
            Direction::Down => Direction::Up,
        }
    }
}
