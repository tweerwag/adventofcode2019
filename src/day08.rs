use std::io::{BufRead, Write};

use crate::util;

const IMAGE_WIDTH: usize = 25;
const IMAGE_HEIGHT: usize = 6;

fn count_digits(layer: &[u32], digit_to_count: u32) -> usize {
    layer
        .iter()
        .filter(|&&digit| digit == digit_to_count)
        .count()
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let line = util::read_line(&mut input);
    let digits = line
        .chars()
        .filter_map(|x| x.to_digit(10))
        .collect::<Vec<_>>();
    let layers = digits
        .chunks_exact(IMAGE_WIDTH * IMAGE_HEIGHT)
        .map(|slice| slice.iter().copied().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    match part {
        1 => {
            let fewest_zeros_layer = layers
                .iter()
                .min_by(|l1, l2| count_digits(l1, 0).cmp(&count_digits(l2, 0)))
                .unwrap();
            writeln!(
                output,
                "{}",
                count_digits(fewest_zeros_layer, 1) * count_digits(fewest_zeros_layer, 2)
            )
            .unwrap();
        }
        2 => {
            let image = layers.iter().fold(
                vec![2; IMAGE_WIDTH * IMAGE_HEIGHT],
                |mut acc, next_layer| {
                    acc.iter_mut().zip(next_layer).for_each(|(x, y)| {
                        if *x == 2 {
                            *x = *y;
                        }
                    });
                    acc
                },
            );

            for y in 0..IMAGE_HEIGHT {
                for x in 0..IMAGE_WIDTH {
                    write!(output, "{}", image[IMAGE_WIDTH * y + x]).unwrap();
                }
                writeln!(output).unwrap();
            }
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day08.txt").unwrap(), "1548");
    }

    #[test]
    fn part2() {
        assert_io_eq!(
            2,
            read_to_string("inputs/day08.txt").unwrap(),
            "0110011110100101001001100
1001010000101001001010010
1000011100110001001010010
1000010000101001001011110
1001010000101001001010010
0110011110100100110010010"
        );
    }
}
