use std::io::{BufRead, Write};

use crate::util;

fn fft_phase_fast(data: &[i64], skip_first: usize) -> Vec<i64> {
    let n = data.len() + skip_first;
    let mut result = vec![0; n - skip_first];
    let mut acc = 0;

    for i in (skip_first..n).rev() {
        let mut runner = 0;
        'runner: loop {
            let start = 2 * runner * (i + 1) + i;
            let end = start + i + 1;
            if start >= n {
                break 'runner;
            }
            //println!("Runner {}, start={}, end={}", runner, start, end);

            let to_add = 2 * runner + 1;
            let to_subtract = to_add + 1;

            let add_start = start;
            let add_end = usize::min(start + to_add, end);
            let sub_start = usize::max(start + to_add, end);
            let sub_end = end + to_subtract;

            for idx in add_start..add_end {
                if idx >= n {
                    break;
                }
                acc += if runner % 2 == 0 {
                    data[idx - skip_first]
                } else {
                    -data[idx - skip_first]
                };
                //println!("-- Add {} (at {}) acc={}", data[idx], idx, acc);
            }

            for idx in sub_start..sub_end {
                if idx >= n {
                    break;
                }
                acc -= if runner % 2 == 0 {
                    data[idx - skip_first]
                } else {
                    -data[idx - skip_first]
                };
                //println!("-- Sub {} (at {}) acc={}", data[idx], idx, acc);
            }

            runner += 1;
        }
        result[i - skip_first] = acc.abs() % 10;
        //println!("Storing at {}: {}", i, result[i]);
    }
    //println!("{:?}", &result);

    result
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    let digits = util::read_line(&mut input)
        .chars()
        .map(|c| c.to_digit(10).map(|x| x as i64))
        .collect::<Option<Vec<_>>>()
        .unwrap();

    match part {
        1 => {
            let mut result = digits;
            for _ in 0..100 {
                result = fft_phase_fast(&result, 0);
            }
            writeln!(
                output,
                "{}",
                result
                    .iter()
                    .copied()
                    .take(8)
                    .fold(0, |acc, digit| acc * 10 + digit)
            )
            .unwrap();
        }
        2 => {
            let mut result = digits.repeat(10000);
            let skip_first = result
                .iter()
                .copied()
                .take(7)
                .fold(0, |acc, digit| acc * 10 + digit) as usize;
            result.drain(0..skip_first);
            for _ in 0..100 {
                result = fft_phase_fast(&result, skip_first);
            }
            writeln!(
                output,
                "{}",
                result
                    .iter()
                    .copied()
                    .take(8)
                    .fold(0, |acc, digit| acc * 10 + digit)
            )
            .unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use std::fs::read_to_string;

    use crate::assert_io_eq;

    use super::*;

    struct FFTPatternIter {
        num_repetitions: usize,
        state: u8,
        cur_repetition: usize,
    }

    impl Iterator for FFTPatternIter {
        type Item = i64;

        fn next(&mut self) -> Option<i64> {
            let ret = match self.state {
                0 => 0,
                1 => 1,
                2 => 0,
                3 => -1,
                _ => unreachable!(),
            };

            if self.cur_repetition < self.num_repetitions {
                self.cur_repetition += 1;
            } else {
                self.cur_repetition = 0;
                self.state = (self.state + 1) % 4;
            }

            Some(ret)
        }
    }

    fn fft_pattern_iter(num_repetitions: usize) -> impl Iterator<Item = i64> {
        assert!(num_repetitions > 0);
        FFTPatternIter {
            num_repetitions: num_repetitions - 1,
            state: 0,
            cur_repetition: 0,
        }
        .skip(1)
    }

    fn fft_phase_slow(data: &mut [i64]) {
        for i in 0..data.len() {
            // Note: this can be done in-place since the fft pat is 0 for 0..i
            data[i] = data
                .iter()
                .copied()
                .zip(fft_pattern_iter(i + 1))
                .map(|(x, y)| x * y)
                .sum::<i64>()
                .abs()
                % 10;
        }
    }

    #[test]
    fn test_fft_pattern_iter() {
        assert_eq!(
            fft_pattern_iter(1).take(8).collect::<Vec<_>>(),
            vec![1, 0, -1, 0, 1, 0, -1, 0]
        );
        assert_eq!(
            fft_pattern_iter(2).take(8).collect::<Vec<_>>(),
            vec![0, 1, 1, 0, 0, -1, -1, 0]
        );
        assert_eq!(
            fft_pattern_iter(3).take(8).collect::<Vec<_>>(),
            vec![0, 0, 1, 1, 1, 0, 0, 0]
        );
    }

    #[test]
    fn test_fft_phase_slow() {
        fn tester(mut data: Vec<i64>, result: Vec<i64>) {
            fft_phase_slow(&mut data);
            assert_eq!(data, result);
        }
        tester(vec![1, 2, 3, 4], vec![2, 5, 7, 4]);
    }

    #[test]
    fn test_fft_phase_fast() {
        fn tester(mut data: Vec<i64>) {
            let fast = fft_phase_fast(&data, 0);
            fft_phase_slow(&mut data);
            assert_eq!(&data, &fast);
        }
        tester(vec![1, 2, 3, 4]);
        tester(vec![1, 2, 3, 4, 5, 6, 7, 8]);
        tester(vec![1; 100]);
        tester(vec![1; 300]);
    }

    #[test]
    fn examples_part1() {
        assert_io_eq!(1, "80871224585914546619083218645595", "24176176");
        assert_io_eq!(1, "19617804207202209144916044189917", "73745418");
        assert_io_eq!(1, "69317163492948606335995924319873", "52432133");
    }

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day16.txt").unwrap(), "40580215");
    }

    #[test]
    fn examples_part2() {
        assert_io_eq!(2, "03036732577212944063491565474664", "84462026");
        assert_io_eq!(2, "02935109699940807407585447034323", "78725270");
        assert_io_eq!(2, "03081770884921959731165446850517", "53553731");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day16.txt").unwrap(), "22621597");
    }
}
