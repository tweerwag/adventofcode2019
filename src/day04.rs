use std::io::{BufRead, Write};

use crate::util::{self, prelude::*};

fn parse_min_max(input: &mut impl BufRead) -> (i64, i64) {
    let line = util::read_line(input);
    let mut iter = line.split('-').filter_map(|x| x.parse::<i64>().ok());
    let min = iter.next().unwrap();
    let max = iter.next().unwrap();
    (min, max)
}

pub fn execute(part: i32, mut input: impl BufRead, mut output: impl Write) {
    match part {
        1 => {
            let (min, max) = parse_min_max(&mut input);
            writeln!(
                output,
                "{}",
                (min..=max)
                    .map(|x| x.digits().collect::<Vec<_>>())
                    .filter(|digits| digits.windows(2).all(|s| s[0] >= s[1])
                        && digits.windows(2).any(|s| s[0] == s[1]))
                    .count()
            )
            .unwrap();
        }
        2 => {
            let (min, max) = parse_min_max(&mut input);
            writeln!(
                output,
                "{}",
                (min..=max)
                    .map(|x| x.digits().collect::<Vec<_>>())
                    .filter(
                        |digits| digits.iter().run_length_encoding().any(|(_, len)| len == 2)
                            && digits.windows(2).all(|s| s[0] >= s[1])
                    )
                    .count()
            )
            .unwrap();
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use crate::assert_io_eq;
    use std::fs::read_to_string;

    #[test]
    fn part1() {
        assert_io_eq!(1, read_to_string("inputs/day04.txt").unwrap(), "460");
    }

    #[test]
    fn part2() {
        assert_io_eq!(2, read_to_string("inputs/day04.txt").unwrap(), "290");
    }
}
